package cz.enehano.training.demoapp.restapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.enehano.training.demoapp.restapi.RestApiApplication;
import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.repository.UserRepository;
import cz.enehano.training.demoapp.restapi.service.UserNotFoundException;
import org.json.JSONArray;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.util.NestedServletException;

import java.net.URI;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes={ RestApiApplication.class })
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest {

    @LocalServerPort
    private int port;

    private String uri;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() throws Exception {
        this.uri = "http://localhost:" + this.port;
    }

    @Test
    public void getAllUsers() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get(new URI(this.uri + "/users")))
                .andExpect(status().isOk())
                .andReturn();

        JSONArray responseJson = new JSONArray(mvcResult.getResponse().getContentAsString());
        assertEquals(4, responseJson.length());
    }

    @Test
    public void getUser() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get(new URI(this.uri + "/users/1")))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        UserDto userDto = mapper.readValue(mvcResult.getResponse().getContentAsString(), UserDto.class);

        assertEquals("Ivan", userDto.getName());
        assertEquals("Ivann", userDto.getLogin());
        assertEquals("ivan@ivann", userDto.getEmail());
        assertEquals("12345", userDto.getPhone());
    }

    @Test
    @WithMockUser(username = "Ivann", password = "1111", roles = "USER")
    public void createUser() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setLogin("TestLogin");
        userDto.setName("TestName");
        userDto.setEmail("test@mail");
        userDto.setPhone("12345");
        userDto.setPassword("TestPassword");

        ObjectMapper mapper = new ObjectMapper();
        MvcResult mvcResult = this.mockMvc.perform(post(new URI(this.uri + "/users"))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(userDto)))
                .andExpect(status().isOk())
                .andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();
        UserDto responseDto = mapper.readValue(jsonResponse, UserDto.class);

        //Test if we saved correct object
        assertEquals(responseDto.getLogin(),userDto.getLogin());
        //Test if object contains creator defined in WithMockUser
        assertEquals(responseDto.getCreatedBy(), "Ivann");

        //Initialy we have 4 objects in db, now it should be 5
        mvcResult = this.mockMvc.perform(get(new URI(this.uri + "/users")))
                .andExpect(status().isOk())
                .andReturn();

        JSONArray responseJson = new JSONArray(mvcResult.getResponse().getContentAsString());
        assertEquals(5, responseJson.length());

        Optional<User> storedUser = this.userRepository.findByLogin("TestLogin");
        assertTrue(storedUser.isPresent());
        assertTrue(checkPassword(userDto.getPassword(), storedUser.get().getPassword()));
    }

    @Test
    @WithMockUser(username = "Ivann", password = "1111", roles = "USER")
    public void updateUser() throws Exception {

        UserDto updatedUser = new UserDto();
        updatedUser.setName("Update");
        updatedUser.setLogin("Jozeff");
        updatedUser.setPhone("12345");
        updatedUser.setEmail("jozef@jozeffUpdate");
        updatedUser.setPassword("2222");

        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(put(new URI(this.uri + "/users/2"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updatedUser)))
                .andExpect(status().isOk());


        Optional<User> storedUser = this.userRepository.findByLogin("Jozeff");
        assertTrue(storedUser.isPresent());
        assertEquals(updatedUser.getName(), storedUser.get().getName());
        assertEquals(updatedUser.getLogin(), storedUser.get().getLogin());
        assertEquals(updatedUser.getEmail(), storedUser.get().getEmail());
        assertTrue(checkPassword(updatedUser.getPassword(), storedUser.get().getPassword()));
    }

    @Test
    @WithMockUser(username = "Ivann", password = "1111", roles = "USER")
    public void deleteUser() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get(new URI(this.uri + "/users")))
                .andExpect(status().isOk())
                .andReturn();

        JSONArray responseJson = new JSONArray(mvcResult.getResponse().getContentAsString());
        int numberOfResultsInDb = responseJson.length();

        //Perform delete and check if number of results is -1
        this.mockMvc.perform(delete(new URI(this.uri + "/users/3")));

        mvcResult = this.mockMvc.perform(get(new URI(this.uri + "/users")))
                .andExpect(status().isOk())
                .andReturn();

        responseJson = new JSONArray(mvcResult.getResponse().getContentAsString());
        assertEquals(numberOfResultsInDb - 1, responseJson.length());

        //Try to find deleted user
        try {
            this.mockMvc.perform(get(new URI(this.uri + "/users/3")))
                    .andExpect(status().isOk())
                    .andReturn();
            fail("We should not be able to find deleted user");
        } catch (NestedServletException | UserNotFoundException expectedException) {
            expectedException.printStackTrace();
        }
    }

    /**
     * Function check entered password with stored hash
     * @param password_plaintext
     * @param stored_hash
     * @return true if password matches the hash
     */
    public  boolean checkPassword(String password_plaintext, String stored_hash) {
        boolean password_verified = false;
        if(null == stored_hash || !stored_hash.startsWith("$2a$"))
            throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

        password_verified = BCrypt.checkpw(password_plaintext, stored_hash);
        return(password_verified);
    }
}