package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.model.User;

import java.util.List;

/**
 * @author Ivan Moscovic on 30.6.2018.
 */
public interface UserServiceInterface {

    /**
     * Get particular user from db.
     * @param id user id
     * @return
     */
    User getUserById(Long id);

    /**
     * Get all users from db.
     * @return
     */
    List<User> getAllUsers();

    /**
     * Create new user in db.
     * @param user
     * @param createdBy
     * @return
     */
    User createUser(User user, String createdBy);

    /**
     * Update existing user in db.
     * @param id
     * @param user
     * @return
     */
    User updateUser(Long id, User user);

    /**
     * Delete existing user from db.
     * @param id
     */
    void deleteUser(Long id);
}
