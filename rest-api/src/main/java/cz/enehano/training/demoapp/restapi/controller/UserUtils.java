package cz.enehano.training.demoapp.restapi.controller;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.model.User;
import org.dozer.DozerBeanMapper;

/**
 * @author Ivan Moscovic on 30.6.2018.
 */
public class UserUtils {

    /**
     * Help function for converting and mapping of objects
     * @param userDto - UserDto instance
     * @return User instance
     */
    public static User convertUserDTOtoUserDAO(UserDto userDto){
        return new DozerBeanMapper().map(userDto, User.class);
    }

    /**
     * Help function for converting and mapping of objects
     * @param user - User instance
     * @return - UserDto instance
     */
    public static UserDto convertUserDAOtoUSerDTO(User user){
        return new DozerBeanMapper().map(user, UserDto.class);
    }
}
