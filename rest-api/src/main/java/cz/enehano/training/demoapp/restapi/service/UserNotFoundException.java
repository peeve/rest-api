package cz.enehano.training.demoapp.restapi.service;

/**
 * @author Ivan Moscovic on 30.6.2018.
 */
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(Long id) {
        super("User with id=" + id + "was not found");
    }

    public UserNotFoundException(String login) {
        super("User with login=" + login + "was not found");
    }

    public UserNotFoundException(Long id, String msg) {
        super("User with id=" + id + "was not found, " + msg);
    }

}
