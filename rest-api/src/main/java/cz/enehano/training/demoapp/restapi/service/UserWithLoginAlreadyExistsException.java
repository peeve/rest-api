package cz.enehano.training.demoapp.restapi.service;

/**
 * @author Ivan Moscovic on 2.7.2018.
 */
public class UserWithLoginAlreadyExistsException extends RuntimeException {

    public UserWithLoginAlreadyExistsException(String login) {
        super("User with login=" + login + "already exists");
    }
}
