package cz.enehano.training.demoapp.restapi.repository;

import cz.enehano.training.demoapp.restapi.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author by Peeve on 30.6.2018.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByLogin(String login);
}
