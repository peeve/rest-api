package cz.enehano.training.demoapp.restapi.dto;

import lombok.Data;

@Data
public class UserDto {

    private Long id;
    private String name;
    private String login;
    private String email;
    private String phone;
    private String password;
    private String createdBy;
}
