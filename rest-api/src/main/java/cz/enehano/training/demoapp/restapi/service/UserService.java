package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Ivan Moscovic on 30.6.2018.
 */

@Service
public class UserService implements UserServiceInterface {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User getUserById(Long id) {
        return this.userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public List<User> getAllUsers() {
        return StreamSupport.stream((this.userRepository.findAll()).spliterator(), false)
                .sorted((o1, o2) -> (o1.getLogin()).compareTo(o2.getLogin()))
                .collect(Collectors.toList());
    }

    @Override
    public User createUser(User user, String createdById) {
        Optional<User> userOpt = this.userRepository.findByLogin(user.getLogin());
        if (userOpt.isPresent()) {
            throw new UserWithLoginAlreadyExistsException(user.getLogin());
        }
        user.setDateOfCreation(LocalDateTime.now());
        user.setCreatedBy(createdById);
        user.setPassword(this.passwordEncoder.encode(user.getPassword()));
        return this.userRepository.save(user);
    }

    @Override
    public User updateUser(Long id, User user) {
        Optional<User> userOpt = this.userRepository.findById(id);
        if(!userOpt.isPresent()) {
            throw new UserNotFoundException(id);
        }
        user.setId(id);
        user.setPassword(this.passwordEncoder.encode(user.getPassword()));
        return this.userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        Optional<User> userOpt = this.userRepository.findById(id);
        if (!userOpt.isPresent()) {
            throw new UserNotFoundException(id);
        }
        this.userRepository.deleteById(id);
    }


}
