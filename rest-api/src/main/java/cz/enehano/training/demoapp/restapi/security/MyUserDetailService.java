package cz.enehano.training.demoapp.restapi.security;

import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.service.UserNotFoundException;
import org.springframework.security.core.userdetails.User.UserBuilder;
import cz.enehano.training.demoapp.restapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Ivan Moscovic on 30.6.2018.
 */
@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) {
        User user = this.userRepository
                .findByLogin(login).orElseThrow(() -> new UserNotFoundException(login));
        UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(login);
        builder.password(user.getPassword());
        builder.roles("USER");

        return builder.build();
    }
}
