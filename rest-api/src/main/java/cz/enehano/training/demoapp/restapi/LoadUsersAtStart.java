package cz.enehano.training.demoapp.restapi;

import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Class for initial upload of users to db, when application starts.
 * @author Ivan Moscovic on 30.6.2018.
 */
@Component
public class LoadUsersAtStart implements CommandLineRunner {

    @Autowired
    private UserService userService;

    @Override
    public void run(String... args) throws Exception {

        User user1 = new User();
        user1.setName("Ivan");
        user1.setLogin("Ivann");
        user1.setPhone("12345");
        user1.setEmail("ivan@ivann");
        user1.setPassword("1111");

        User user2 = new User();
        user2.setName("Jozef");
        user2.setLogin("Jozeff");
        user2.setPhone("12345");
        user2.setEmail("jozef@jozeff");
        user2.setPassword("2222");

        User user3 = new User();
        user3.setName("Milan");
        user3.setLogin("Milann");
        user3.setPhone("12345");
        user3.setEmail("milan@milann");
        user3.setPassword("3333");

        User user4 = new User();
        user4.setName("Stefan");
        user4.setLogin("Stefann");
        user4.setPhone("12345");
        user4.setEmail("stefan@stefann");
        user4.setPassword("4444");

        userService.createUser(user1, null);
        userService.createUser(user2, null);
        userService.createUser(user3, null);
        userService.createUser(user4, null);
    }

}
