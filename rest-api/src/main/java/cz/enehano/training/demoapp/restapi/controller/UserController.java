package cz.enehano.training.demoapp.restapi.controller;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * RestController for users
 * @author Ivan Moscovic on 30.6.2018
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Test url
     * @return greetings!
     */
    @RequestMapping("/hello")
    public String sayHi() {
        return "Hi";
    }

    /**
     * Load all users from db.
     * @return
     */
    @RequestMapping(method=RequestMethod.GET, value="/users")
    public List<UserDto> getAllUsers() {
        List<User> users = this.userService.getAllUsers();
        return users.stream().map(user -> UserUtils.convertUserDAOtoUSerDTO(user)).collect(Collectors.toList());
    }

    /**
     * Get particular user from db.
     * @param id - id of user
     * @return
     */
    @RequestMapping(method=RequestMethod.GET, value="/users/{id}")
    public UserDto getUser(@PathVariable Long id) {
        User user = this.userService.getUserById(id);
        return UserUtils.convertUserDAOtoUSerDTO(user);
    }

    /**
     * Save new user to db.
     * @param userDto
     * @return
     */
    @RequestMapping(method= RequestMethod.POST, value="/users")
    public UserDto createUser(@RequestBody UserDto userDto) throws AuthenticationException {

        org.springframework.security.core.userdetails.User currentUser = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated()) {
            try {
                currentUser = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
            } catch (ClassCastException x) {
                throw new AuthenticationCredentialsNotFoundException("Anonymous user tried to create new user");
            }
            this.userService.createUser(UserUtils.convertUserDTOtoUserDAO(userDto),
                    currentUser != null ? currentUser.getUsername() : null);
            userDto.setCreatedBy(currentUser != null ? currentUser.getUsername() : null);
            return userDto;
        }
        throw new AuthenticationCredentialsNotFoundException("User was not successfully authenticated");
    }

    /**
     * Update existing user in db.
     * @param userDto
     * @param id
     * @return
     */
    @RequestMapping(method=RequestMethod.PUT, value="/users/{id}")
    public UserDto updateUser(@RequestBody UserDto userDto, @PathVariable Long id) {
        this.userService.updateUser(id, UserUtils.convertUserDTOtoUserDAO(userDto));
        return userDto;
    }

    /**
     * Delete user from db.
     * @param id
     */
    @RequestMapping(method=RequestMethod.DELETE, value="/users/{id}")
    public void deleteUser(@PathVariable Long id) {
        this.userService.deleteUser(id);
    }

}
